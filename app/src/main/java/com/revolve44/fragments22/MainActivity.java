package com.revolve44.fragments22;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.revolve44.fragments22.storage.SharedPref;
import com.revolve44.fragments22.ui.manager.AddStationActivity;
import com.revolve44.fragments22.ui.dashboard.ConsoleFragment;
import com.revolve44.fragments22.ui.home.HomeFragment;
import com.revolve44.fragments22.ui.manager.ListofStations;
import com.revolve44.fragments22.utils.Api;
import com.revolve44.fragments22.utils.CalcEngine;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import static android.content.ContentValues.TAG;

/*
created in 2020
TODO: Attention! after every changes in code you must write in history directory what you change.
Current version 0.2.52
 */

/*
History of app
from 2020/07/17

0.2.52
i change code in manager folder. i fix error NPE: at the same time appears two rows with data of one station

0.2.56
add in mainactivity when app started - it shouldt run all fragments, - only load mapactivity

 */
public class MainActivity extends AppCompatActivity {

    final Fragment fragment1 = new HomeFragment();
    final Fragment fragment2 = new ConsoleFragment();
    final FragmentManager fm = getSupportFragmentManager();
    Fragment active = fragment1;

    private Api api = new Api();// now is correct may response
    private CalcEngine engine = new CalcEngine();
    //private Context contextx;
    private boolean firstStart;

    private FirebaseAnalytics mFirebaseAnalytics;


    public void FirstLaunch(){

        try {
            // Obtain the FirebaseAnalytics instance.
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        }catch (Exception e){
            Log.d("FIREBASE", "ERROR");
        }


        SharedPreferences prefs = getSharedPreferences("keeper", Context.MODE_PRIVATE);
        firstStart = prefs.getBoolean("firstStart", true);
        Log.d(TAG, "FirstLaunch: "+ firstStart);
        if (firstStart) {
            Intent intent = new Intent(this, AddStationActivity.class); // here bug been , relation with extra-
            startActivity(intent);

//            Intent intent = new Intent(MainActivity.this, AddStationActivity.class);
//            //startActivityForResult(intent, ADD_NOTE_REQUEST);
//            startActivity(intent);
        }
    }



    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPref.initPreferences(this);// send context to method in class SharedPref
        FirstLaunch();
        setContentView(R.layout.activity_main);



        if (!firstStart){
            BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.bottom_navigation);
            navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
            //I added this if statement to keep the selected fragment when rotating the device
            if (savedInstanceState == null) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new HomeFragment()).addToBackStack(null).commit();
            }

            //fm.beginTransaction().replace(R.id.fragment_container, fragment3, "3").hide(fragment2).commit();
            fm.beginTransaction().add(R.id.fragment_container, fragment2, "2").hide(fragment2).commit();
            fm.beginTransaction().add(R.id.fragment_container,fragment1, "1").commit();
            LaunchPad();
        }

    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    public void LaunchPad(){
        api.startcall();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                engine.launchmaster();
            }
        }, 1000);


    }


    //switcher of fragmnets, he help for switching without loss filled form in fragments
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fm.beginTransaction().hide(active).show(fragment1).commit();
                    active = fragment1;
                    return true;

                case R.id.navigation_dashboard:
                    fm.beginTransaction().hide(active).show(fragment2).commit();
                    active = fragment2;
                    return true;
            }
            return false;
        }
    };

    public void addpvsys(View view) {
        Intent intent = new Intent(this, ListofStations.class);
        startActivity(intent);
    }

    public void toast3(View view) {
        Snackbar.make(findViewById(android.R.id.content), "Sunset", Snackbar.LENGTH_LONG).show();
    }

    public void toast2(View view) {
        Snackbar.make(findViewById(android.R.id.content), "Sunshine duration", Snackbar.LENGTH_LONG).show();
    }

    public void toast1(View view) {
        Snackbar.make(findViewById(android.R.id.content), "Sunset", Snackbar.LENGTH_LONG).show();
    }
}

