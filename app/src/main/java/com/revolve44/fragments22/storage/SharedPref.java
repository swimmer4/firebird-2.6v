package com.revolve44.fragments22.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SharedPref {

    private static SharedPreferences preferences;

    private SharedPref(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void initPreferences(Context context) {
        new SharedPref(context);
    }

    public static Float getTemp(Context context) {
        return preferences.
                getFloat("temp", 0);
    }

    public static void setTemp(Float tempy, Context context) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putFloat("temp", tempy).apply();
        editor.apply();
    }
//////////////// Chosen Station/////////////////////////////////////////////////////////////////////
    public static void setMainStation(String Name, int NominalPower, Float latitude, Float longitude, Context context ){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("name", Name);
        editor.putInt("nominalpower", NominalPower);
        editor.putFloat("lat", latitude);
        editor.putFloat("lon", longitude);
        editor.apply();
    }
    public static Float getLat(Context context) {
        return preferences.
                getFloat("lat", 0);
    }
    public static Float getLon(Context context) {
        return preferences.
                getFloat("lon", 0);
    }

    public static String getName(Context context) {
        return preferences.
                getString("name", "-");
    }

    public static int getNominal(Context context) {
        return preferences.
                getInt("nominalpower", 1);
    }
///////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void setSecondaryData (String city, Long UnixCurrentTime, Long unixSunset, Long unixSunrise, Long GMT,Float cloud, Float wind, Float pressure, Float humidity,  Context context ){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("city", city);
        editor.putLong("unixcurrent", UnixCurrentTime);
        editor.putLong("unixsunset", unixSunset);
        editor.putLong("unixsunrise", unixSunrise);
        editor.putLong("gmt", GMT);
        editor.putFloat("cloud", cloud);
        editor.putFloat("wind", wind);
        editor.putFloat("press", pressure);
        editor.putFloat("humidity", humidity);
        editor.apply();
    }

    public static void setTertiaryData (String sunset, String sunrise, int SunPeriod, String solarhoursString,  Context context ){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("sunset", sunset);
        editor.putString("sunrise", sunrise);
        editor.putInt("sunperiod", SunPeriod);
        editor.putString("solarhoursString", solarhoursString);
        editor.apply();
    }

    public static String getCity(Context context) {
        return preferences.
                getString("city", "-");
    }


    public static Long getUnixCurrent(Context context) {
        return preferences.
                getLong("unixcurrent", 0);
    }

    public static Long getUnixSunset(Context context) {
        return preferences.
                getLong("unixsunset", 0);
    }
    public static Long getUnixSunrise(Context context) {
        return preferences.
                getLong("unixsunrise", 0);
    }

    public static Long getGMT(Context context) {
        return preferences.
                getLong("gmt", 1);
    }

    public static Float getCloud(Context context) {
        return preferences.
                getFloat("cloud", 0);
    }

    public static Float getWind(Context context) {
        return preferences.
                getFloat("wind", 0);
    }
    public static Float getPressure(Context context) {
        return preferences.
                getFloat("press", 0);
    }
    public static Float getHumidity(Context context) {
        return preferences.
                getFloat("humidity", 0);
    }


    public static String getSunset(Context context) {
        return preferences.
                getString("sunset", "-");
    }
    public static String getSunrise(Context context) {
        return preferences.
                getString("sunrise", "-");
    }

    public static void setCurrentPower(int CurrentPower, Context context) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("currentpower", CurrentPower).apply();
        editor.apply();
    }
    public static int getCurrentPower(Context context) {
        return preferences.
                getInt("currentpower", 0);
    }
    public static int getSunPeriod(Context context) {
        return preferences.
                getInt("sunperiod", 0);
    }
    public static String getsolarhoursString(Context context) {
        return preferences.
                getString("solarhoursString", "-");
    }

////////////////////////////////////////////////////Chart/////////////////////////////////////////////
    public static void setjsonDataMap(String jsonDataMap, Context context) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("json1", jsonDataMap).apply();
        editor.apply();
    }
    public static String getjsonDataMap(Context context) {
        return preferences.
                getString("json1", "-");
    }

    public static void setJsonForChart (String json, String json2,  Context context ){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("legend", json);
        editor.putString("value", json2);
        editor.apply();
    }

    public static String getChartLegend(Context context) {
        return preferences.
                getString("legend", "-");
    }

    public static String getChartValue(Context context) {
        return preferences.
                getString("value", "-");
    }
////////////////////////////////////////////////////////////////Chart////////////////////////////////

    public static Boolean getFar(Context context) {
        return preferences.
                getBoolean("tempFah", false);
    }

    public static void setFar(boolean tempFahrenheit, Context context) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("tempFah", tempFahrenheit).apply();
        editor.apply();
    }
//    public static void setjsonDataMap(String jsonDataMap, Context context) {
//        SharedPreferences.Editor editor = preferences.edit();
//        editor.putString("json1", jsonDataMap).apply();
//        editor.apply();
//    }
//    public static String getjsonDataMap(Context context) {
//        return preferences.
//                getString("json1", "-");
//    }


//    public static String getSunrise(Context context) {
//        return preferences.
//                getString("sunrise", "-");
//    }
//
//    public static int getSunrise(Context context) {
//        return preferences.
//                getString("sunrise", "-");
//    }

}
