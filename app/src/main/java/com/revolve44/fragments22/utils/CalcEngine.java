package com.revolve44.fragments22.utils;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.revolve44.fragments22.storage.SharedPref;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.TimeZone;

import static android.content.ContentValues.TAG;

public class CalcEngine {
    //Variables

    private float temp2;
    private float result;
    // variable to hold context
    private Context context;
    private float NominalPower;
    private float CurrentPower;
    private int CurrentPowerInt;

    private String solarhoursString;

    private String sunset;
    private String sunrise;
    private String hournowStr;

    //public String jsonString;
    private LinkedList<String> Legendzero = new LinkedList<>();
    private LinkedList<Integer> Valuezero = new LinkedList<>();

    String json;
    String json2;

    int w =0; // for timemanipulations
    int s = 0;

    private int SunPeriod = 0;

    //get from api
    private long GMT;
    private float cloud;
    //public long UTCtime;
    private long UnixCurrentTime;
    private long unixSunrise;
    private long unixSunset;

    private LinkedHashMap<Long, Float> dataMap = new LinkedHashMap<>();
    Gson gson = new Gson();



    public void launchmaster(){
        refreshdata();
        MarsCore();
        TimeManipulation();
        Log.d(TAG, "launchmaster: " + temp2+ " temp 3 is " );
        saveData();

    }

    private void refreshdata(){
        //reinizilization variables

        GMT = SharedPref.getGMT(context);
        UnixCurrentTime = SharedPref.getUnixCurrent(context);
        unixSunrise = SharedPref.getUnixSunrise(context);
        unixSunset = SharedPref.getUnixSunset(context);
        cloud = SharedPref.getCloud(context);

        NominalPower = (float) SharedPref.getNominal(context);


        String storedHashMapString = SharedPref.getjsonDataMap(context); // get HashMap, now is String, after that will be an hashmap
        //java.lang.reflect.Type type = new TypeToken<HashMap<String, String>>(){}.getType();
        Type type = new TypeToken<LinkedHashMap<Long, Float>>() {}.getType();
        dataMap = gson.fromJson(storedHashMapString, type);//]]]

    }

    private void MarsCore(){
        if (cloud >-1 ){
            CurrentPower = NominalPower - NominalPower*(cloud/100)*0.8f;
        }else{
            CurrentPower = 1;
            Log.d(TAG, "MarsCore: "+ "ERROR");
        }
    }

    private void TimeManipulation (){
        Log.d("Lifecycle -> method "," timemanipulations ");
        Log.d("Timemaipulation method "," UTC -> "+ UnixCurrentTime+ " unixSunrise "+ unixSunrise+ " unixSet "+ unixSunset + "GMT "+ GMT);
        if (UnixCurrentTime>1 & unixSunrise>1) {
            ////////////////////////////////////////////////////
            //     Time zone & unix sunrise/sunset            //
            //      Here we define human time                 //
            ////////////////////////////////////////////////////
            long timestamp = UnixCurrentTime + GMT;
            long timestamp2 = unixSunrise + GMT;
            long timestamp3 = unixSunset + GMT;



            //UTCtime = System.currentTimeMillis(); // Here i have been problem coz i multipled on 1000L UTC time:(
            String zeroPlace1 = "";
            String zeroPlace2 = "";
            String zeroPlace3 = "";
            String zeroPlace4 = "";

            /////////////////////////////
            /////////////////////////////
            long day = timestamp / 86400;
            //
            long hourinSec = (timestamp - day * 86400); //hours in sec
            long hour = hourinSec / 3600; // hr
            //
            long minutesinSec = hourinSec - hour * 3600; // minutes in sec
            long minutes = minutesinSec / 60;
            //
            hournowStr = String.valueOf(hour);
            /////////////////////////////////
            /////////////////////////////////
            long day2 = timestamp2 / 86400;

            //
            long hourinSec2 = (timestamp2 - day2 * 86400); //hours in sec
            long hour2 = hourinSec2 / 3600; // hr
            if (hour2 < 10) {
                zeroPlace1 = "0";
            }
            //
            long minutesinSec2 = hourinSec2 - hour2 * 3600; // minutes in sec
            long minutes2 = minutesinSec2 / 60;
            if (minutes2 < 10) {
                zeroPlace2 = "0";
            }
            //
            sunrise = zeroPlace1 + hour2 + ":" + zeroPlace2 + minutes2;
            /////////////////////////////////////
            ////////////////////////////////////
            long day3 = timestamp3 / 86400;

            //
            long hourinSec3 = (timestamp3 - day3 * 86400); //hours in sec
            long hour3 = hourinSec3 / 3600; // hr
            if (hour3 < 10) {
                zeroPlace3 = "0";
            }
            //
            long minutesinSec3 = hourinSec3 - hour3 * 3600; // minutes in sec
            long minutes3 = minutesinSec3 / 60;
            if (minutes3 < 10) {
                zeroPlace4 = "0";
            }
            //
            sunset = zeroPlace3 + hour3 + ":" + zeroPlace4 + minutes3;

            Log.d("TIMEST >", sunrise + "and sunset " + sunset);
            Log.d("TIMEST >", timestamp + "and  " + timestamp2);

            //////////////////////////////////
            int hournow = (int) hour;
            int hourRise = (int) hour2;
            int hourSet = (int) hour3;

            int sector = (hourSet - hourRise) / 5;

            //set Sun Position
            if (hournow > hourSet) {
                SunPeriod = 0;
                //Toast.makeText(this, "NIGHT", Toast.LENGTH_SHORT).show();
            } else if (hournow > hourSet - sector) {
                //Toast.makeText(this, "sunset", Toast.LENGTH_SHORT).show();
                SunPeriod = 5;
                //0.6
                CurrentPower = (float) (CurrentPower*0.6);

            } else if (hournow > hourSet - 2 * sector) {
                //Toast.makeText(this, "135", Toast.LENGTH_SHORT).show();
                SunPeriod = 4;
                //0.8
                CurrentPower = (float) (CurrentPower*0.8);

            } else if (hournow > hourSet - 3 * sector) {
                //Toast.makeText(this, "90", Toast.LENGTH_SHORT).show();
                SunPeriod = 3;
                //1

            } else if (hournow > hourSet - 4 * sector) {
                //Toast.makeText(this, "45", Toast.LENGTH_SHORT).show();
                SunPeriod = 2;
                //0.8
                CurrentPower = (float) (CurrentPower*0.8);

            } else if (hournow > hourRise) {
                //Toast.makeText(this, "sunrise", Toast.LENGTH_SHORT).show();
                SunPeriod = 1;
                //0.6
                CurrentPower = (float) (CurrentPower*0.6);

            } else {
                SunPeriod = 0;
//            Toast.makeText(this, "NIGHT", Toast.LENGTH_SHORT).show();
            }

            CurrentPowerInt = Math.round(CurrentPower);
            if (SunPeriod == 0) {
                CurrentPowerInt = 0;
            }

            Log.d("Timemanipulations END> ", "Sunperiod ->" + SunPeriod + " hournow " + hournow);

            Log.d("Datamap 2>>>>>", "" + dataMap);


            ////////////////////////////////////////////////////
            solarhoursString = String.valueOf(hourSet - hourRise);
            Log.d("@@@@@@@@@@@@", "0 Solarhour:  "+solarhoursString);

            //Log.d("##########", " "+sunrise+" "+sunset + " unix -> " +unixSunrise + " GMT is ->" + GMT +" TZ is -> "+ finalblank);
            String zeroPlace5 = "";
            String zeroPlace6 = "";
            //set GMT in human view
            int timeGMT = (int) (GMT/ 3600);
            TimeZone tz = TimeZone.getTimeZone("GMT"+timeGMT);

            int a = 0;
            for (Map.Entry<Long, Float> entry : dataMap.entrySet()) {
                long key = ((entry.getKey()) / 1000L) + GMT;
                float value = (entry.getValue());
                zeroPlace5 = "";
                zeroPlace6 = "";

                // Set current GMT
                /* debug: is it local time? */
                Log.d("Time zone: ", tz.getDisplayName());
                /* date formatter in local timezone */
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MMMM");
                sdf.setTimeZone(tz);
                /* print your timestamp and double check it's the date you expect */
                String date = sdf.format(new Date(key * 1000));

                //String date = DateFormat.format("dd-MMMM", key*1000L).toString();


                long day4 = key / 86400;

                //
                long hourinSec4 = (key - day4 * 86400); //hours in sec
                long hour4 = hourinSec4 / 3600; // hr
                if (hour4 < 10) {
                    zeroPlace5 = "0";
                }
                //
                long minutesinSec4 = hourinSec4 - hour4 * 3600; // minutes in sec
                long minutes4 = minutesinSec4 / 60;
                if (minutes4 < 10) {
                    zeroPlace6 = "0";
                }
                //-2:-59 > 21:00
                String ModernTime = zeroPlace5 + hour4 + ":" + zeroPlace6 + minutes4;

                //Log.d("Hashmap test loop -> ", "key : "+ key + " value : "+ value);
                int transitTime = (int) hour4;
                Log.d("Hashmap test loop -> ", "transittime : " + transitTime + " hoursunset : " + hourSet);
                Log.d("Loopz ", ModernTime+ " and value " + value);
                if (transitTime > hourSet) {
                    //0
                    //corvette.put(ModernTime, value*0f);
                    Legendzero.add(ModernTime+" "+date);
                    Valuezero.add((int) (value*0f));
                } else if (transitTime > hourSet - sector) {
                    //0.6
                    //corvette.put(ModernTime, value*0.6f);
                    Legendzero.add(ModernTime+" "+date);
                    Valuezero.add((int) (value*0.6f));
                } else if (transitTime > hourSet - 2 * sector) {
                    //0.8
                    //corvette.put(ModernTime, value*0.8f);
                    Legendzero.add(ModernTime+" "+date);
                    Valuezero.add((int) (value*0.8f));
                } else if (transitTime > hourSet - 3 * sector) {
                    //1
                    //corvette.put(ModernTime, value);
                    Legendzero.add(ModernTime+" "+date);
                    Valuezero.add((int) value);
                } else if (transitTime > hourSet - 4 * sector) {
                    //0.8
                    //corvette.put(ModernTime, value*0.8f);
                    Legendzero.add(ModernTime+" "+date);
                    Valuezero.add((int) (value*0.8f));
                } else if (transitTime > hourSet - 5 * sector) {
                    //0.6
                    //corvette.put(ModernTime, value*0.6f);
                    Legendzero.add(ModernTime+" "+date);
                    Valuezero.add((int) (value*0.6f));
                } else {
                    //0
                    //corvette.put(ModernTime, value*0f);
                    Legendzero.add(ModernTime+" "+date);
                    Valuezero.add((int) (value*0f));
                }
                a++;
                Log.d(" loop ", a + " times ");
                Log.d("Loop after of this", ModernTime+ " and value " + value);
            }
            w++;
        }

    }

    private void saveData(){
        Log.d("Value and legend 3>>>>>", ""+Legendzero+ "< ][ >"+Valuezero);
        //////////////////////////////////////////////////////////////////////////
        //                    convert to gson and save                          //
        //////////////////////////////////////////////////////////////////////////
        if (Legendzero.size()>1 & s<1){
            Gson gson = new Gson();
            SharedPref.setJsonForChart(gson.toJson(Legendzero),gson.toJson(Valuezero), context );
            s++;
        }
        SharedPref.setCurrentPower(CurrentPowerInt,context);
        SharedPref.setTertiaryData(sunset, sunrise, SunPeriod, solarhoursString, context);
        Log.d(TAG, "saveData: calc engine  "+ CurrentPowerInt +" "+ CurrentPower);
    }
}
