package com.revolve44.fragments22.utils;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.google.gson.Gson;
import com.revolve44.fragments22.storage.SharedPref;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Api {
    public static String BaseUrl = "https://api.openweathermap.org/";
    public static String CITY = "Moscow";

    public static String AppId = "4e78fa71de97f97aa376e42f2f1c99cf";
    public static String MC = "&units=metric&appid=";

    private CalcEngine engine2 = new CalcEngine();

    //CalcEngine

    public static String lat = "55.78";
    public static String lon = "49.12";
    public static String metric = "metric";


    //Variables
    public float NominalPower;//????????????????????????????????
    public float CurrentPower;
    public int CurrentPowerInt;
    public float cloud;
    public float windF;
    public int windI;
    //public float temp;

    public String desription;
    public float temperature = 12f;
    public float humidity;
    public boolean tempScale;

    public long unixSunrise;
    public long unixSunset;

    public String city;
    public String country;

    private Context mContext;

    private LinkedHashMap<Long, Float> dataMap = new LinkedHashMap<>();
    private long TimeHashMap;
    private float CurrentPowerHashMap;

    int z = 0; // for retrofit -


    @RequiresApi(api = Build.VERSION_CODES.N)
    public void startcall(){
        try {

        }catch (Exception e){

        }
        NominalPower = (float) SharedPref.getNominal(mContext);
        lon = String.valueOf(SharedPref.getLon(mContext));
        lat = String.valueOf(SharedPref.getLat(mContext));

        getCurrentData();
        //endprocess();
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    void getCurrentData() {
        Log.d("Lifecycle -> method ", " getCurrentdata ");

        Log.d("Lifecycle -> method ", " latitude " + lat + lon);
        OkHttpClient.Builder okhttpClientBuilder = new OkHttpClient.Builder();//for create a LOGs
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor(); //for create a LOGs
        logging.setLevel(HttpLoggingInterceptor.Level.BODY); //for create a LOGs
        okhttpClientBuilder.addInterceptor(logging); //for create a LOGs

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okhttpClientBuilder.build()) //for create a LOGs
                .build();
        WeatherService service = retrofit.create(WeatherService.class);
        //Call<WeatherResponse> call = service.getCurrentWeatherData(CITY, metric, AppId);
        Call<WeatherResponse> call = service.getCurrentWeatherData(lat, lon, metric, AppId);
        call.enqueue(new Callback<WeatherResponse>() {
            @Override
            public void onResponse(@NonNull Call<WeatherResponse> call, @NonNull Response<WeatherResponse> response) {
                if (response.code() == 200) {
                    WeatherResponse weatherResponse = response.body();
                    assert weatherResponse != null;

                    //main variables
                    SharedPref.setTemp(weatherResponse.main.temp,mContext);
                    SharedPref.setSecondaryData(
                            weatherResponse.name,
                            (long) weatherResponse.dt,
                            weatherResponse.sys.sunset,
                            weatherResponse.sys.sunrise,
                            weatherResponse.timezone,
                            weatherResponse.clouds.all,
                            weatherResponse.wind.speed,
                            weatherResponse.main.pressure,
                            weatherResponse.main.humidity, mContext);
                }
            }


            @Override
            public void onFailure(@NonNull Call<WeatherResponse> call, @NonNull Throwable t) {
                Log.e("ERROR", " On Retrofit");
            }
        });
        Call<WeatherForecastResponse> forecastCall = service.getDailyData(lat, lon, metric, AppId);
        forecastCall.enqueue(new Callback<WeatherForecastResponse>() {
            @Override
            public void onResponse(@NonNull Call<WeatherForecastResponse> forecastCall, @NonNull Response<WeatherForecastResponse> response) {
                if (response.code() == 200) {
                    WeatherForecastResponse weatherResponse = response.body();
                    assert weatherResponse != null;
                    ArrayList<WeatherResponse> list = weatherResponse.list;

                    if (dataMap.size() == 0){

                        Log.d("z -1 ", z+" ");

                        for(WeatherResponse wr: list){

                            if (z<=20){
                                CurrentPowerHashMap = NominalPower - NominalPower * (wr.clouds.all / 100)*0.8f; // чисто разницу лучше не оставлять, а домножанать на 0.8 например чтобы при макс. облач. не было нуля

                                TimeHashMap = (long) wr.dt * 1000;

                                dataMap.put(TimeHashMap, CurrentPowerHashMap);
                                Log.d("Datamap 1->", TimeHashMap+" "+ CurrentPowerHashMap);
                                z++;
                            }
                        }
                        Log.d("Datamap 1>>>>>", ""+dataMap);
                    }
                    //convert to string using gson
                    Gson gson = new Gson();
                    SharedPref.setjsonDataMap(gson.toJson(dataMap), mContext);
                }
            }

            @Override
            public void onFailure(@NonNull Call<WeatherForecastResponse> forecastCall, @NonNull Throwable t) {
                Log.e("ERROR", "retrofit");
            }
        });

    }
}
