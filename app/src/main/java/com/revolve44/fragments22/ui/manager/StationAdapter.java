package com.revolve44.fragments22.ui.manager;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.revolve44.fragments22.R;
import com.revolve44.fragments22.storage.Station;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class StationAdapter extends RecyclerView.Adapter<StationAdapter.StationHolder> {
    //private Context context;
    private List<Station> stations = new ArrayList<>();
    private int selectedPosition = -1;// no selection by default***

    private String selectedNamePosition = "";

    private String nameofStation;
    private int nominalPowerofStation;
    private Context context;

    private View.OnClickListener mOnItemClickListener;

    @NonNull
    @Override
    public StationHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.note_item, parent, false);

        //Log.d(TAG, "onBindViewHolder: TTT"+ stations.get(1).toString());
//        System.out.println(stations + " 777");
         context = parent.getContext();

        SharedPreferences prefs = context.getSharedPreferences(
                "keeper", Context.MODE_PRIVATE);
        selectedPosition = prefs.getInt("selected", selectedPosition);

//        SharedPreferences prefs = PreferenceManager
//                .getDefaultSharedPreferences(this);
        Log.d(TAG, "444 onBindViewHolder: 0");
//        selectedPosition = prefs.getInt("selected", 1); // getting Integer
//
//        SharedPreferences.Editor editor = prefs.edit();
//        editor.putString("name",nameofStation);
//        editor.putInt("selected", selectedPosition);
//        editor.apply();

        return new StationHolder(itemView);
    }
    @Override
    public void onBindViewHolder(@NonNull final StationHolder holder, final int position) {
       // Station currentStation2 = stations.get(0);
      //  Log.d("TYT", "run: rock" + currentStation2.getTitle());// here i may call for a position
        Log.d(TAG, "444 onBindViewHolder: 1");

        SharedPreferences prefs = context.getSharedPreferences(
                "keeper", Context.MODE_PRIVATE);
        selectedPosition = prefs.getInt("selected", selectedPosition);
        selectedNamePosition = prefs.getString("selectedname", ""); // construction works
        Station currentStation4 = stations.get(position);

        // Position started from 0
        if(selectedNamePosition.equals(currentStation4.getName())){
            //holder.setMyStation.setChecked(true);
            holder.textViewSelected.setText("Selected");
            holder.textViewSelected.setTextColor(ContextCompat.getColor(context, R.color.greenSelect));
        }
        else{
            //holder.setMyStation.setChecked(false);
            holder.textViewSelected.setText("not selected");
            holder.textViewSelected.setTextColor(ContextCompat.getColor(context, R.color.colorCardView));
        }
        // set data from Database
        Station currentStation = stations.get(position);

        holder.textViewTitle.setText(currentStation.getName());
        holder.textViewCoordination.setText("lat: "+currentStation.getLatitude() + " ; lon: " + currentStation.getLongitude());
        holder.textViewNominalPower.setText(String.valueOf(currentStation.getNominalpower())+"Watt");


        Log.i(TAG, "StationHolder: LIST " + currentStation.toString());



        Log.d(TAG, "onBindViewHolder: position is" + selectedPosition );
        Log.d(TAG, "444 onBindViewHolder: 2");
    }

    //TODO: Step 2 of 4: Assign itemClickListener to your local View.OnClickListener variable
    public void setOnItemClickListener(View.OnClickListener itemClickListener) {
        Log.d(TAG, "setOnItemClickListener: its works!");
        mOnItemClickListener = itemClickListener;
        //notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return stations.size();
    }

    public Station getStationAt(int position){
        return stations.get(position);
    }
    public void setStations(List<Station> stations) {
        this.stations = stations;
        notifyDataSetChanged(); // recyclerview refresh
    }
    class StationHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView textViewTitle;
        private TextView textViewCoordination;
        private TextView textViewNominalPower;
        private TextView textViewSelected;
        // Started first

        public StationHolder(final View itemView) {
            super(itemView);

            Log.d(TAG, "444 onBindViewHolder: 3");

            textViewTitle = itemView.findViewById(R.id.text_view_title);
            textViewCoordination = itemView.findViewById(R.id.text_view_coordination);
            textViewNominalPower = itemView.findViewById(R.id.text_view_nominalpower);
            textViewSelected = itemView.findViewById(R.id.text_view_selected);
            //setMyStation = itemView.findViewById(R.id.checkBox);


            //TODO: Step 3 of 4: setTag() as current view holder along with
            // setOnClickListener() as your local View.OnClickListener variable.
            // You can set the same mOnItemClickListener on multiple views if required
            // and later differentiate those clicks using view's id.
            itemView.setTag(this);
            itemView.setOnClickListener(mOnItemClickListener);


            Log.d(TAG, "444 onBindViewHolder: 4");
        }


        @Override
        public void onClick(View view) {
            Log.i(TAG, "onClick: test1 " + textViewTitle.getText().toString());
            ;
        }
    }

    private void saveSelect(int selectedPosition) {
        //SharedPreferences.Editor editor2 = context.getSharedPreferences("myPrefs", Context.MODE_PRIVATE).edit();
        SharedPreferences prefs = context.getSharedPreferences(
                "keeper", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("selected", selectedPosition);
        editor.apply();
    }
}
