package com.revolve44.fragments22.ui.home;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.snackbar.Snackbar;
import com.revolve44.fragments22.MainActivity;
import com.revolve44.fragments22.R;
import com.revolve44.fragments22.storage.SharedPref;
import com.revolve44.fragments22.ui.home.recyclerview.Model;
import com.revolve44.fragments22.ui.home.recyclerview.MultiViewTypeAdapter;
import com.revolve44.fragments22.utils.Api;
import com.revolve44.fragments22.utils.CalcEngine;

import java.util.ArrayList;
import java.util.Random;

import static android.content.ContentValues.TAG;

public class HomeFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private Api api = new Api();// now is correct may response
    private CalcEngine engine = new CalcEngine();

    private RelativeLayout SkyLayout;

    private int ANIMATION_DURATION = 3000;

    // a c = is start
    public float a = -400; // start X
    public float c = 60; // start Y

    public float b = 150;// to X
    public float d = 150;// to Y

    public int width;
    public int height;

    public int wind;

    public int xSun;
    public int ySun;

    //SupportMapFragment mapView;

    int random_num;

    Float nominalpower;
    String city;

    String sunrise;
    String sunset;
    String solarhoursString;
    private int SunPeriod;

    TextView CityView;
    TextView NominalPower;
    TextView CurrentPower;

    TextView SunriseView;
    TextView SunsetView;
    TextView SolarHoursView;

    TextView WindView;
    ImageView HotView;

    ImageView mobile;
    ImageView lamp;
    ImageView tv;
    ImageView teapot;
    ImageView oven;

    public int currentPower;

    public int amortization = 0;

    private SwipeRefreshLayout mSwipeRefreshLayout;
    RecyclerView mRecyclerView;
    MultiViewTypeAdapter adapter;
    /*
    Two state:
    1. Startup app
    Make API Response + load SharedPreference
    2. Runtime app
    Load SharedPreference
     */

    private boolean firstStartf;


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("Lifecycle of Fragment->", " Fragment -> onCreate launch ");



    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root =  inflater.inflate(R.layout.fragment_home, container, false); //added View root
        Log.d("Lifecycle of Fragment->", " Fragment -> onCreateView launch ");
        //((MainActivity)getActivity()).runforecast();

        CurrentPower = root.findViewById(R.id.Forecast_number);
        NominalPower = root.findViewById(R.id.NominalView);
        CityView = root.findViewById(R.id.cityView);

        SunriseView = root.findViewById(R.id.timesunrise);
        SunsetView = root.findViewById(R.id.timesunset);
        SolarHoursView = root.findViewById(R.id.solardayhr);

        WindView = root.findViewById(R.id.WindSpeed);

        mobile = root.findViewById(R.id.mobile);
        lamp = root.findViewById(R.id.lamp);
        tv = root.findViewById(R.id.tv);
        teapot = root.findViewById(R.id.teapot);
        oven = root.findViewById(R.id.oven);


        mSwipeRefreshLayout = root.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener((SwipeRefreshLayout.OnRefreshListener) this);

        mSwipeRefreshLayout.getNestedScrollAxes();

        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_red_light);
        try {
            SharedPreferences prefs = getActivity().getSharedPreferences("keeper", Context.MODE_PRIVATE);
            firstStartf = prefs.getBoolean("firstStart", true);
        }catch (Exception e){

        }


        //////////////////////////////////////////////////////////
        //              recyclerview                            //
        //////////////////////////////////////////////////////////

        mRecyclerView = (RecyclerView) root.findViewById(R.id.recyclerView);
        ArrayList<Model> list2= new ArrayList<>();
        //list2.add(new Model())
        list2.add(new Model(Model.TEXT_TYPE,"",0));
        try {
            list2.add(new Model(Model.GRAPH_TYPE,"",0));
        }catch (Exception e){
            Log.d("MyError -> ", " add graphtype");
        }

        final MultiViewTypeAdapter adapter = new MultiViewTypeAdapter(list2,getActivity());
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);


        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(adapter);

        ////////////////////////////////////////////////////////////////////////////////
//        button.setOnClickListener(new View.OnClickListener()
//        {
//            @SuppressLint("SetTextI18n")
//            @Override
//            public void onClick(View v)
//            {
//                LaunchForecast();
//            }
//        });
        if (!firstStartf){
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    LaunchForecast();
                }
            }, 1500);
            //Random
            int min = 1;
            int max = 50;

            Random r = new Random();
            random_num = r.nextInt(max - min + 1) + min;
            //Random end
            //sunny day
        }


        return root;
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    public void LaunchForecast (){
        Log.d("Lifecycle -> method "," LaunchForecast ");
        final ProgressDialog dialog = ProgressDialog.show(getActivity(), "",
                getString(R.string.DialogLoad), true);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @SuppressLint("SetTextI18n")
            @Override
            public void run() {

                Log.d("@@@@@@@@@@@@", "2 Solarhour:  "+solarhoursString);

                CityView.setText(""+ SharedPref.getCity(getActivity()));
                CityView.setSelected(true);
                CurrentPower.setText(""+ SharedPref.getCurrentPower(getActivity()));;
                WindView.setText(""+ Math.round(SharedPref.getWind(getActivity())));
                NominalPower.setText(""+ SharedPref.getNominal(getActivity())+ " W");
                SunPeriod = SharedPref.getSunPeriod(getActivity());

                SunsetView.setText(""+ SharedPref.getSunset(getActivity()));
                SunriseView.setText(""+ SharedPref.getSunrise(getActivity()));
                SolarHoursView.setText(""+ SharedPref.getsolarhoursString(getActivity()) + " hr");


                try {
                    if (Integer.parseInt(sunrise.substring(0, sunrise.length() - 3))>8 ){

                        if (amortization<2){
                            LaunchForecast();
                            amortization++;
                        }
                    }
                }catch (Exception e){
                    Log.d("ERROR try/catch", " sunrise ");

                }

                DisplayMetrics displaymetrics = new DisplayMetrics();
                getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
                height = displaymetrics.heightPixels;
                width = displaymetrics.widthPixels;

                try {
                    //Restart recyclerview
                    ArrayList<Model> list2= new ArrayList<>();
                    list2.add(new Model(Model.TEXT_TYPE,getString(R.string._48_hour_power_generation_forecast_from_solar_panels),0));
                    list2.add(new Model(Model.GRAPH_TYPE,"",0));
                    adapter = new MultiViewTypeAdapter(list2,getActivity());
                    final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

                    mRecyclerView.setLayoutManager(linearLayoutManager);
                    mRecyclerView.setItemAnimator(new DefaultItemAnimator());
                    mRecyclerView.setAdapter(adapter);
                }catch (Exception e){
                    Log.d(TAG, "run: recycler"+" ERROR");
                }

                dialog.dismiss();
                setvisualelements();
            }
        }, 4000);
    }

    private void setvisualelements(){

        currentPower = SharedPref.getCurrentPower(getContext());
        if (currentPower >15){
            mobile.setVisibility(View.VISIBLE);
        }
        if (currentPower >60){
            lamp.setVisibility(View.VISIBLE);
        }
        if (currentPower >300){
            tv.setVisibility(View.VISIBLE);
        }
        if (currentPower > 1500){
            teapot.setVisibility(View.VISIBLE);
        }
        if (currentPower > 2000){
            oven.setVisibility(View.VISIBLE);
        }

        sundinamics();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 5s = 5000ms

                rotateFan();
            }
        }, 4500);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d("Lifecycle of Fragment->", " Fragment -> onViewCreated launch ");
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onResume() {
        super.onResume();
        Log.d("Lifecycle of Fragment->", " Fragment -> onResume launch ");

        String sharedcity = SharedPref.getCity(getActivity());
        String curcity = String.valueOf(CityView.getText());

        //Check
        if (!firstStartf & !sharedcity.isEmpty() & !sharedcity.equals(curcity)){
            onRefresh();
        }
        Log.d("###########","in Resume - City is " +city + nominalpower);
    }

    public void rotateFan() {
        ImageView imageX= (ImageView) requireView().findViewById(R.id.fan);
        RotateAnimation rotate = new RotateAnimation(360, 0, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);

        // Bofort scale
        if (wind == 0){
            rotate.setDuration(9000);
        }else if (wind>=1 && wind <= 3) {
            rotate.setDuration(5000);
        }else if (wind>=2 && wind <=5){
            rotate.setDuration(3000);
        }else if (wind>=6 && wind <= 10){
            rotate.setDuration(1500);
        }else if (wind>=11){
            rotate.setDuration(900);
        }
        rotate.setRepeatCount(Animation.INFINITE);

        rotate.setInterpolator(new LinearInterpolator());
        imageX.startAnimation(rotate);

    }

    public void sundinamics(){

        ImageView sun= getView().findViewById(R.id.ivSun);
        float x1= 0; // X
        float x2= 0;
        float y1= 0; // Y
        float y2= 0;
        String night = "#20242B";
        String day = "#769CDD";
        String evening = "#BB584F";
        String SkyColour1 = "#AFE333";//<-
        String SkyColour2 = "#AFE333";//<-
        Log.d(TAG, "sundinamics: sunperiod "+SunPeriod);
        if (SunPeriod==1){
            //sunrise();
            x1= -320;
            y1 = 250;

            x2 =-width/10f;
            y2 = 80;
            //////////////////
            SkyColour1 = night;
            SkyColour2 = day;

        }else if (SunPeriod == 2){
            //45
            x1= -320;
            y1 = 250;

            x2 = -width/15f;
            y2 = -50;
            //////////////////
            SkyColour1 = night;
            SkyColour2 = day;
        }else if (SunPeriod == 3){
            //90
            x1= -320;
            y1 = 250;

            x2 = width/3.8f;
            y2 = -100;
            //////////////////
            SkyColour1 = night;
            SkyColour2 = day;
        }else if (SunPeriod == 4){
            //135
            x1= width/3.8f;
            y1 = -350;

            x2 = (width/5f)*3;
            y2 = -55;
            SkyColour1 = night;
            SkyColour2 = day;
        }else if (SunPeriod == 5){
            //135
            x1= width/3.8f;
            y1 = -350;

            x2 = (width/5f)*3;
            y2 = 80;
            SkyColour1 = day;
            SkyColour2 = evening;

        }else{
            SkyColour1 = night;
            SkyColour2 = night;
            sun.setVisibility(View.INVISIBLE);
            x1= -600;
            y1 = -600;

            x2 = -400;
            y2 = -400;
        }



        //darken sky
        SkyLayout = (RelativeLayout) getView().findViewById(R.id.SkyLayout);
        ValueAnimator skyAnim =
                ObjectAnimator.ofInt(SkyLayout, "backgroundColor",
                        Color.parseColor(SkyColour1),
                        Color.parseColor(SkyColour2));

        skyAnim.setDuration(ANIMATION_DURATION);
        skyAnim.setEvaluator(new ArgbEvaluator());
        skyAnim.start();

        TranslateAnimation animation1 = new TranslateAnimation(x1, x2,y1,y2 );
        animation1.setDuration(3000);
        animation1.setFillAfter(true);
        sun.startAnimation(animation1);
        try {
            Snackbar.make(getActivity().findViewById(android.R.id.content),"Selected PV system: "+ SharedPref.getName(getContext()),Snackbar.LENGTH_SHORT).show();

        }catch (Exception e){

        }
    }






    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onRefresh() {
        ((MainActivity) requireActivity()).LaunchPad();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                LaunchForecast();



                try {
                    mRecyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    mRecyclerView.setAdapter(adapter);

                }catch (Exception e){
                    Snackbar.make(getActivity().findViewById(android.R.id.content),"Selected pv system: "+ SharedPref.getName(getContext()),Snackbar.LENGTH_SHORT).show();
                }

                mSwipeRefreshLayout.setRefreshing(false);
            }
        }, 1500);


    }
}
