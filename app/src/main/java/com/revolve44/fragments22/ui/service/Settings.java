package com.revolve44.fragments22.ui.service;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;

import com.revolve44.fragments22.R;
import com.revolve44.fragments22.storage.SharedPref;

public class Settings extends AppCompatActivity {
    boolean tempFahrenheit;
    CheckBox checkImperial;
    Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        checkImperial = findViewById(R.id.checkImperial);

        tempFahrenheit = SharedPref.getFar(ctx);
        checkImperial.setChecked(tempFahrenheit);

        checkImperial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                SharedPreferences.Editor editor = sharedPreferences.edit();
//
//                editor.putBoolean("tempFah", ((CheckBox) view).isChecked());
//                editor.commit();
                SharedPref.setFar(((CheckBox) view).isChecked(),ctx);


                if (checkImperial.isChecked()) {

                } else {

                }
            }
        });

        checkImperial.setChecked(tempFahrenheit);

        Log.d("after Sh pref CHECKBOX ", tempFahrenheit+ " ");


    }
}
