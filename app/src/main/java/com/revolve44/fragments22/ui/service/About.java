package com.revolve44.fragments22.ui.service;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.revolve44.fragments22.R;

public class About extends AppCompatActivity {
    private int step =10;

    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_about);
    }


    public void writetodev(View view) {

        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"info@revolna.com"});
        i.putExtra(Intent.EXTRA_SUBJECT, "i have a question or suggestion");
        i.putExtra(Intent.EXTRA_TEXT   , "So, ...");
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(About.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    public void gosite(View view) {
        goToUrl ( "http://revolna.com/");
    }

    public void goinstagram(View view) {
        goToUrl ( "https://www.instagram.com/revolna_workshop/");
    }

    public void gotelegram(View view) {
        goToUrl ( "https://t.me/solarpan");

    }

    private void goToUrl (String url) {
        Uri uriUrl = Uri.parse(url);
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }


    public void twitter(View view) {
        goToUrl ( "https://twitter.com/revolnaenergy");
    }

    public void firebirdMaster(View view) {

        try {
            Bundle bundle = new Bundle();
            bundle.putInt(FirebaseAnalytics.Param.ITEM_ID, step);
            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        }catch (Exception e){
            Log.d("FIREBASE", "ERROR2");
        }



        step--;
        if (step<1){
            goToUrl ( "https://twitter.com/ProfFeynman/status/1277669286794817537");

        }
        Snackbar.make(findViewById(android.R.id.content),step + " steps to the Easter Egg ",Snackbar.LENGTH_LONG).show();

    }
}

