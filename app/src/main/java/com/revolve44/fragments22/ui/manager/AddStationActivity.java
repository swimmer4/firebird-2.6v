package com.revolve44.fragments22.ui.manager;

import androidx.fragment.app.FragmentActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.snackbar.Snackbar;
import com.revolve44.fragments22.R;
import com.revolve44.fragments22.storage.StationDatabase;

public class AddStationActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerDragListener {

    private GoogleMap mMap;
    public LatLng lol;
    Marker marker;
    double latitude;
    double longitude;
    public Float NominalPower = 0.0f;

    public String Latitude;
    public String Longitude;

    Boolean check = false;

    LatLng MYLOCATION =  new LatLng (latitude, longitude);

    LinearLayout Loader;

    TextView textView;

    EditText inputnominalpower;
    TextView Latview;
    TextView LonView;

    Button saverx;

    boolean tempFahrenheit;
    public StationDatabase noteDatabase;
    //public final String TABLE_NAME =
    public static final String EXTRA_NAME =
            "EXTRA_NAME";
    public static final String EXTRA_NOMINALPOWER =
            "EXTRA_DESCRIPTION";//?
    public static final String EXTRA_LATITUDE =
            "EXTRA_LAT";
    public static final String EXTRA_LONGITUDE =
            "EXTRA_LON";
    public static final String EXTRA_NEW_ELEMENT = "EXTRA_NEW_ELEMENT";
    private EditText NameInput;
    private EditText NominalPowerInput;
    private EditText LatInput;
    private EditText LonInput;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);

//        (getActionBar()).setTitle("Add new PV System ");
        SharedPreferences prefs = getSharedPreferences("keeper", Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = prefs.edit();
        boolean firstStart = prefs.getBoolean("firstStart", true);
        boolean showagain = prefs.getBoolean("showagain", true);
        Log.d("boolean ", "onCreate: " + showagain+firstStart);
        if (firstStart | showagain){
            // <---- run your one time code here
            AlertDialog alertDialog = new AlertDialog.Builder(AddStationActivity.this).create();
            alertDialog.setTitle(getString(R.string.DialogMap1));
            alertDialog.setMessage(getString(R.string.DialogMap2));
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,"Don't show again",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            editor.putBoolean("showagain", false);
                            editor.apply();
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        }

//        editor.putBoolean("firstStart", false);
//        editor.apply();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        NameInput = findViewById(R.id.edit_text_title);
        NominalPowerInput = findViewById(R.id.edit_text_nominalpower);
        Latview = findViewById(R.id.latview);
        LonView = findViewById(R.id.lonview);
        saverx = findViewById(R.id.buttonx);




    }

//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater menuInflater = getMenuInflater();
//        menuInflater.inflate(R.menu.add_note_menu, menu);
//        return true;
//    }
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.save_note:
//
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }

    /** ШАБЛОННЫЙ ГУГОЛОВСКИЙ КОМЕНТАРИЙ ПО ПОВОДУ ИХ КАРТ, РЕЛАКС. донт ворри
     * просто оставил почитать
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        SharedPreferences sharedPreferences = getSharedPreferences("MasterSave", MODE_PRIVATE);

        latitude = sharedPreferences.getFloat("latitudeF",(float)latitude);
        longitude = sharedPreferences.getFloat("longitudeF",(float)longitude);
        check = sharedPreferences.getBoolean("CHECK_SAVINGS",check);
        mMap = googleMap;


        if(check = true){
            LatLng resumedPosition = new LatLng(latitude,longitude);

            marker = googleMap.addMarker(new MarkerOptions()
                    .position(resumedPosition)
                    .draggable(true)
            );
            mMap.setOnMarkerDragListener(this); // bridge for connect marker with methods located below
            mMap.animateCamera(CameraUpdateFactory.newLatLng(resumedPosition)); // move camera to current position
        }else{
            marker = googleMap.addMarker(new MarkerOptions()
                    .position(MYLOCATION)
                    .draggable(true)
            );
            mMap.setOnMarkerDragListener(this); // bridge for connect marker with methods located below
            mMap.animateCamera(CameraUpdateFactory.newLatLng(MYLOCATION)); // move camera to current position

        }

        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        //setUpTracking();
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng latLng) {
                // create marker
                MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longitude)).title("Hello Maps");

                // adding marker
                mMap.addMarker(marker);

                // Creating a marker
                MarkerOptions markerOptions = new MarkerOptions();

                // Setting the position for the marker
                markerOptions.position(latLng);

                // Setting the title for the marker.
                // This will be displayed on taping the marker
                markerOptions.title(latLng.latitude + " : " + latLng.longitude);

                // Clears the previously touched position
                mMap.clear();

                // Animating to the touched position
                mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

                //Placing a marker on the touched position
                mMap.addMarker(markerOptions);
                // get coord
                latitude = latLng.latitude;
                longitude = latLng.longitude;

                Latview.setText("lat: \n"+Math.round(latitude * 100.0) / 100.0);
                LonView.setText("lon: \n"+Math.round(longitude * 100.0) / 100.0);

                Snackbar.make(AddStationActivity.this.findViewById(android.R.id.content),"Coord: "+latitude+" "+ longitude,Snackbar.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void onMarkerDragStart(Marker marker) {
        Snackbar.make(AddStationActivity.this.findViewById(android.R.id.content),"onMarkerDragStart ",Snackbar.LENGTH_SHORT).show();

    }

    @Override
    public void onMarkerDrag(Marker marker) {
        Snackbar.make(AddStationActivity.this.findViewById(android.R.id.content),"onMarkerDrag ",Snackbar.LENGTH_SHORT).show();


    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        MYLOCATION = marker.getPosition();
        textView.setText(lol+"");
        Snackbar.make(AddStationActivity.this.findViewById(android.R.id.content),"My coordinates: " + lol,Snackbar.LENGTH_SHORT).show();

    }


    public void help(View view) {
        AlertDialog alertDialog = new AlertDialog.Builder(AddStationActivity.this).create();
        alertDialog.setTitle(getString(R.string.DialogMap1));
        alertDialog.setMessage(getString(R.string.DialogMap2));
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void adder(View view) {
        try {
            String name = String.valueOf(NameInput.getText().toString());
            int nominalpower = Integer.parseInt(NominalPowerInput.getText().toString());
            float lat = (float) latitude;
            float lon = (float) longitude;
            int newelement = 1;

            Log.d("ppp", "saveNote: " + lon+" "+lat + name+" "+nominalpower);
            if (name.isEmpty() | nominalpower ==0 | lat ==0 | lon ==0){
                Toast.makeText(this, "Please fill all forms, and set location", Toast.LENGTH_SHORT).show();

            }else{
                SharedPreferences prefs = getSharedPreferences("keeper", Context.MODE_PRIVATE);
                final SharedPreferences.Editor editor = prefs.edit();
                editor.putBoolean("firstStart", false);
                editor.apply();
                Log.d("TEG", "adder: pizdec");
                Log.d("buthert", "saveNote: launch" );

                Intent data = new Intent(getBaseContext(), ListofStations.class);
                data.putExtra(EXTRA_NAME, name);
                data.putExtra(EXTRA_NOMINALPOWER, nominalpower);
                data.putExtra(EXTRA_LATITUDE, lat);
                data.putExtra(EXTRA_LONGITUDE, lon);
                data.putExtra(EXTRA_NEW_ELEMENT, newelement);
                startActivity(data);
            }

        }catch (Exception e){
            Log.d("ERROR", "adder: ");
            Toast.makeText(this, "Please fill all forms, and set location", Toast.LENGTH_SHORT).show();


        }
    }

    @Override
    public void onBackPressed() {
        // Do Here what ever you want do on back press;
        int newelement = getIntent().getIntExtra(AddStationActivity.EXTRA_NEW_ELEMENT, 0);
        SharedPreferences prefs = getSharedPreferences("keeper", Context.MODE_PRIVATE);
//        final SharedPreferences.Editor editor = prefs.edit();
        boolean firstStartx = prefs.getBoolean("firstStart", true);

        if (!firstStartx) {

//            Intent intent = new Intent(AddStationActivity.this, ListofStations.class);
//            startActivity(intent);
            super.onBackPressed(); // default method to back
        }else{
            Toast.makeText(this, "Please fill all forms, and set location", Toast.LENGTH_SHORT).show();

        }
    }
}
