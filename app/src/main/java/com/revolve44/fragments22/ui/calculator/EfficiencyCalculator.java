package com.revolve44.fragments22.ui.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.revolve44.fragments22.R;
import com.revolve44.fragments22.storage.SharedPref;

import java.text.DecimalFormat;
import java.util.Locale;

public class EfficiencyCalculator extends AppCompatActivity {
    Context contextR;

    float PricekWh;
    float CostStation;
    float TimesOffGrid;
    float CostFood;
    float PaybackPeriod;

    float NominalPower;
    String Language;
    public String Currency;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_efficiency_calculator);

        Language = Locale.getDefault().getDisplayLanguage();

        //((MainActivity) Objects.requireNonNull(getActivity())).TimeManipulations();

        Float nominalpower = (float) SharedPref.getNominal(contextR);
        final TextView NominalView2= findViewById(R.id.NominalView2);

        final EditText PriceEnergyINPUT = findViewById(R.id.price_per_kWh);
        final EditText PriceofStationINPUT = findViewById(R.id.price_of_station);
        final EditText GridINPUT = findViewById(R.id.times_grid_off);
        final EditText CostFoodINPUT = findViewById(R.id.cost_food);

        final TextView PaybackView = findViewById(R.id.paybackView);
        final Switch Checkgrid = findViewById(R.id.checkgrid);
        Button button = findViewById(R.id.tocalc);


        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {


                NominalPower = (float) SharedPref.getNominal(contextR);
                Float nominalpower = (float) SharedPref.getNominal(contextR);

                NominalView2.setText("" + nominalpower+" W - is nominal power of your solar panels");
                NominalView2.setSelected(true);
            }
        }, 2000);



        // Spinner element
        String [] values =
                {"$","€","₽",};
        Spinner spinner = (Spinner) findViewById(R.id.Currency);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, values);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spinner.setAdapter(adapter);


        TimesOffGrid = 365; // default - user do not have connection to the power grid
        Checkgrid.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if (isChecked) {
                    // user have connection to power grid
                    try {
                        TimesOffGrid = Float.parseFloat(GridINPUT.getText().toString());
                    }catch (Exception e){
                        TimesOffGrid = 0;
                        GridINPUT.setText("0");
                    }
                } else {
                    // user don`t have connection to power grid
                    TimesOffGrid = 365;
                }
            }
        });


        /////////////////////////////////////////////////////////////////////
        //               Calculator Engine                                 //
        /////////////////////////////////////////////////////////////////////
        button.setOnClickListener(new View.OnClickListener()
        {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v)
            {
                try {
                    PricekWh = Float.parseFloat(PriceEnergyINPUT.getText().toString());
                    CostStation = Float.parseFloat(PriceofStationINPUT.getText().toString());
                    try {
                        CostFood = Float.parseFloat(CostFoodINPUT.getText().toString());
                    }catch (Exception e){
                        Toast.makeText(contextR,"Food empty",Toast.LENGTH_SHORT).show();
                    }
                    if (NominalPower>0) {
                        if (Currency=="$"){
                            PaybackPeriod = CostStation/((NominalPower/1000) * (PricekWh/100) * 5 * 365 + TimesOffGrid * CostFood);
                            DecimalFormat df = new DecimalFormat("##.##");
                            PaybackView.setText(df.format(PaybackPeriod)+" years");
                            //Toast.makeText(getActivity(),"$year ->  "+PaybackPeriod,Toast.LENGTH_SHORT).show();

                        }else if (Currency=="€"){

                            PaybackPeriod = CostStation/ ((NominalPower/1000) * (PricekWh/100) * 5 * 365 + TimesOffGrid * CostFood);
                            DecimalFormat df = new DecimalFormat("##.##");
                            PaybackView.setText(df.format(PaybackPeriod)+" years");

                        }else if (Currency=="₽"){

                            PaybackPeriod = CostStation/((NominalPower/1000) * PricekWh * 5 * 365 + TimesOffGrid * CostFood);
                            DecimalFormat df = new DecimalFormat("##.##");
                            PaybackView.setText(df.format(PaybackPeriod)+" years");
                            //Toast.makeText(getActivity(),"Food "+CostFood + "Times grid "+ TimesOffGrid,Toast.LENGTH_SHORT).show();
                            //Toast.makeText(getActivity(),"РУБЛЬCurrency "+PaybackPeriod+" NomPow "+ NominalPower,Toast.LENGTH_SHORT).show();

                        }
                    }else{
                        Toast.makeText(contextR,"Make sure you input a NOMINAL POWER ",Toast.LENGTH_SHORT).show();
                    }




                }catch (Exception e){
                    Toast.makeText(contextR,"Please correct fill above forms ",Toast.LENGTH_SHORT).show();
                }

                //CalculatePayback();
            }


        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos,
                                       long id) {
                Currency = adapterView.getItemAtPosition(pos).toString();
                Toast.makeText(adapter.getContext(),
                        Currency + " is chosen",
                        Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Currency = "$";
            }
        });

        NominalView2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Toast.makeText(contextR,"This is the rated (nominal) power of your solar panels ",Toast.LENGTH_SHORT).show();
            }

        });

    }
}
