package com.revolve44.fragments22.ui.manager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.revolve44.fragments22.MainActivity;
import com.revolve44.fragments22.R;
import com.revolve44.fragments22.storage.SharedPref;
import com.revolve44.fragments22.storage.Station;
import com.revolve44.fragments22.storage.StationDao;
import com.revolve44.fragments22.storage.StationViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ListofStations extends AppCompatActivity {
    private StationViewModel stationViewModel;

    private DrawerLayout mDrawer;
    private Toolbar toolbarx;
    private NavigationView nvDrawer;
    private StationDao stationDao2;

    private List<Station> stations2 = new ArrayList<>();
    private List<Station> stations3 = new ArrayList<>();
    private int position;

    private String selectedname = "";

    String TAG = "check";
    RecyclerView recyclerView;
    Context contextL;
    int delta = 0;
    int timesadd = 0;

    /*
    Need:
    Name
    Nominal Power
    Longitude
    Latitude
     */

    public static final int ADD_NOTE_REQUEST = 1;
    public void FirstLaunch(){
        SharedPreferences prefs = getSharedPreferences("keeper", Context.MODE_PRIVATE);
        boolean firstStart = prefs.getBoolean("firstStart", true);
        if (firstStart) {
            Intent intent = new Intent(ListofStations.this, AddStationActivity.class);
            //startActivityForResult(intent, ADD_NOTE_REQUEST);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirstLaunch();
        final SharedPreferences prefs = getSharedPreferences("keeper", Context.MODE_PRIVATE);
        setContentView(R.layout.listofstation);


        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        myToolbar.setTitle("List of my PV systems: ");
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        final StationAdapter adapter = new StationAdapter();
        recyclerView.setAdapter(adapter);
        //TODO: Step 1 of 4: Create and set OnItemClickListener to the adapter.
        adapter.setOnItemClickListener(onItemClickListener);

        FloatingActionButton buttonAddNote = findViewById(R.id.button_add_note);
        buttonAddNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListofStations.this, AddStationActivity.class);
                startActivityForResult(intent, ADD_NOTE_REQUEST);
            }
        });

        //NoobHelper();

        ///////////////////////////////////////////////////////////////////////////////////
        stationViewModel = ViewModelProviders.of(this).get(StationViewModel.class);
        stationViewModel.getAllStations().observe(this, new Observer<List<Station>>() {
            @Override
            public void onChanged(@Nullable final List<Station> stations ) {
                adapter.setStations(stations);


                try {
                    final int listSize = stations.size();
                    stations2 = stations;
                    if (stations2.size()>0 & stations2.size()<2){
                        SharedPreferences prefs = getSharedPreferences("keeper", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        //editor.putString("name",nameofStation);
                        editor.putInt("selected", 0);
                        editor.apply();
                        recyclerView.setAdapter(adapter);
                        NoobHelper();
                    }else {
                        position = prefs.getInt("selected", position);
                        //update RecyclerView
                        //stations.get(position);
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                try {
                                    Station currentStation = stations.get(position);
                                    Log.i(TAG, "onChanged: test2" + stations+ "Lenght is "+ listSize+" position is "+ position+ " onChanged " + currentStation.getName());

                                    AccessData();
                                }catch (Exception r){

                                }
                            }
                        }, 600);
                    }


                }catch (Exception e){

                }

            }
        });
        //Tools to support deleting rows
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }


            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                int swipedpos = viewHolder.getAdapterPosition(); // get pos which we swipe

                SharedPreferences prefs = getSharedPreferences("keeper", Context.MODE_PRIVATE);
                selectedname = prefs.getString("selectedname","");
                Station currentStation5 = stations2.get(swipedpos); // connect to db

                //Logs:
                Log.i(TAG, "onSwiped: test5 "+ position + " and swiped pos "+ viewHolder.getAdapterPosition()+ " direction "+ direction+ "get old pos is "+viewHolder.getOldPosition()+ "get item id is " + viewHolder.getItemId());
                Log.d(TAG, "onSwipedd: "+ adapter.getStationAt(viewHolder.getAdapterPosition()));
                Log.d(TAG, "onSwiped888: "+ currentStation5.getName());

                if (currentStation5.getName().equals(selectedname)){ // check -> swiped station equal with selected?

                    recyclerView.scrollToPosition(position);
                    adapter.notifyDataSetChanged();
                    Snackbar.make(findViewById(android.R.id.content),"Сannot be deleted" ,Snackbar.LENGTH_SHORT).show();

                }else{
                    stationViewModel.delete(adapter.getStationAt(viewHolder.getAdapterPosition()));
                    adapter.notifyDataSetChanged();

                    Snackbar.make(findViewById(android.R.id.content),"Station deleted" ,Snackbar.LENGTH_SHORT).show();
                }


                               //Snackbar.make(findViewById(android.R.id.content),"Station deleted" ,Snackbar.LENGTH_SHORT).show();

            }
        }).attachToRecyclerView(recyclerView);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms


                // You can directly print your ArrayList
                System.out.println(stations2 + "   999");
                // Station station = new Station("hey",null, 10);
                // System.out.println(station.toString() + "   999");
            }
        }, 2000);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);

        catchdata();


    }
    // Catch data from AddStationActivity
    //AND SAVE IN DATABASE
    private void catchdata() {

        //getting from another activity
        //Intent data = new Intent(getBaseContext(), ListofStations.class);
        String name = getIntent().getStringExtra(AddStationActivity.EXTRA_NAME);
        int nominalpower = getIntent().getIntExtra(AddStationActivity.EXTRA_NOMINALPOWER, 100);
        float latitude = getIntent().getFloatExtra(AddStationActivity.EXTRA_LATITUDE,0f);
        float longitude = getIntent().getFloatExtra(AddStationActivity.EXTRA_LONGITUDE,0f);
//            String description = data.getStringExtra(AddStationActivity.EXTRA_LATITUDE);
//            int priority = data.getIntExtra(AddStationActivity.EXTRA_PRIORITY);
        int newelement2 = getIntent().getIntExtra(AddStationActivity.EXTRA_NEW_ELEMENT, 0);
        Log.d(TAG, "onActivityResult: "+ longitude+ " long "+ latitude);
        // sending catching data

        if (newelement2 >=1 & timesadd<1){
            Station station = new Station( name,  nominalpower,  latitude,  longitude); // init constructor and class entity
            stationViewModel.insert(station); // call method from viewmodel
            timesadd++;
            Log.d("ListOfst catchdata()", "timesadd is "+timesadd);
        }

    }

    // Listener when item clicked
    //just transition data from another activity
    private View.OnClickListener onItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //TODO: Step 4 of 4: Finally call getTag() on the view.
            // This viewHolder will have all required values.
            SharedPreferences prefs = getSharedPreferences("keeper", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
//            int oldposition = prefs.getInt("selected", -1);
            RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) view.getTag();
            position = viewHolder.getAdapterPosition();
            // get parameters of station, when user clicked
            Station currentStation3 = stations2.get(position);



            //editor.putString("name",nameofStation);
            editor.putString("selectedname",currentStation3.getName() );
            editor.apply();
            Objects.requireNonNull(recyclerView.getAdapter()).notifyDataSetChanged();//? may NULL


            Snackbar.make(findViewById(android.R.id.content),"Selected: "+ currentStation3.getName() +", "+ currentStation3.getNominalpower() +" W",Snackbar.LENGTH_SHORT).show();
            SharedPref.setMainStation(currentStation3.getName(),currentStation3.getNominalpower(),currentStation3.getLatitude(), currentStation3.getLongitude(),contextL);
            if (stations2.size()<2 & stations2.size()>0){
                Intent intent = new Intent(ListofStations.this, MainActivity.class);
                startActivity(intent);
            }
            delta++;
            Toast.makeText(ListofStations.this, "You Clicked: " + position + " "+ stations2.size() + "Delta is "+delta, Toast.LENGTH_SHORT).show();
        }
    };

    public void NoobHelper(){

        Objects.requireNonNull(recyclerView.getAdapter()).notifyDataSetChanged();//? may NULL

        // get parameters of station, when user clicked
        Station currentStation3 = stations2.get(position);
        Snackbar.make(findViewById(android.R.id.content),"Selected: "+ currentStation3.getName() +", "+ currentStation3.getNominalpower() +" W",Snackbar.LENGTH_SHORT).show();
        SharedPref.setMainStation(currentStation3.getName(),currentStation3.getNominalpower(),currentStation3.getLatitude(), currentStation3.getLongitude(),contextL);

    }

    public void AccessData(){
        Station currentStation2 = stations2.get(position);
        //Station currentStation3 = stations3.get(2);+ currentStation2.getName()
        //String a = String.valueOf(stations2.get(1));
        Snackbar.make(findViewById(android.R.id.content),"This is list of your PV stations or systems ",Snackbar.LENGTH_LONG).show();

        Log.i(TAG, "AccessData: " +currentStation2.getName() + "and second is " );
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        Log.d(TAG, "onActivityResult() called with: requestCode = [" + requestCode + "], resultCode = [" + resultCode + "], data = [" + data + "]");
//
////            //getting from another activity
////            String name = data.getStringExtra(AddStationActivity.EXTRA_NAME);
////            int nominalpower = data.getIntExtra(AddStationActivity.EXTRA_NOMINALPOWER, 100);
////            float latitude = data.getFloatExtra(AddStationActivity.EXTRA_LATITUDE,0f);
////            float longitude = data.getFloatExtra(AddStationActivity.EXTRA_LONGITUDE,0f);
//////            String description = data.getStringExtra(AddStationActivity.EXTRA_LATITUDE);
//////            int priority = data.getIntExtra(AddStationActivity.EXTRA_PRIORITY);
////            Log.d(TAG, "onActivityResult: "+ longitude+ " long "+ latitude);
////            // sending catching data
////            Station station = new Station( name,  nominalpower,  latitude,  longitude); // init constructor and class entity
////            stationViewModel.insert(station); // call method from viewmodel
//
//
////            Toast.makeText(this, "Station saved", Toast.LENGTH_SHORT).show();
////
////            Toast.makeText(this, "Station not saved, please check that you have completed all forms", Toast.LENGTH_LONG).show();
////
//    }


    //Buttons to back
    @Override
    public void onBackPressed() {
        // Do Here what ever you want do on back press;
        int newelement = getIntent().getIntExtra(AddStationActivity.EXTRA_NEW_ELEMENT, 0);


        if (delta > 0 || newelement==1) {
            Intent intent = new Intent(ListofStations.this, MainActivity.class);
            startActivity(intent);
        }else{
            super.onBackPressed(); // default method to back
        }
    }

    public void getback1(View view) {
        // Do Here what ever you want do on back press;
        int newelement = getIntent().getIntExtra(AddStationActivity.EXTRA_NEW_ELEMENT, 0);

        //Check - we have changes positions in this activity - yes or no?
        if (delta > 0 || newelement==1) {
            Intent intent = new Intent(ListofStations.this, MainActivity.class);
            startActivity(intent);
        }else{
            super.onBackPressed(); // default method to back
        }
    }
}
